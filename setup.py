import os
from pathlib import Path
from setuptools import setup, find_packages

readme_path = Path(os.path.dirname(__file__)) / "README.md"

setup(
    name="resopt",
    version="0.0.1",
    author="Richard Eames",
    author_email="github@naddiseo.ca",
    description="A Result and Option datatype for Python",
    license="BSD",
    keywords="Result type option",
    packages=find_packages(),
    package_data={"resopt": ["py.typed"]},
    long_description=readme_path.read_text(),
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Intended Audience :: Developers",
        "License :: OSI Approved :: BSD License",
        "Programming Language :: Python :: 3.7",
        "Programming Language :: Python :: 3.8",
    ],
    zip_safe=False,
)
