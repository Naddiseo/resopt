from unittest import TestCase
from resopt import Some, Null, Ok, Err


class TestOption(TestCase):
    def test_eq(self):
        self.assertEqual(Some(1), Some(1))
        self.assertNotEqual(Some(2), Some(1))
        self.assertEqual(Null(), Null())
        self.assertEqual(Null(), Null)
        self.assertNotEqual(Some(1), Null())

        s = Some(1)
        self.assertEqual(s, s)
        n = Null
        self.assertEqual(n, n)

    def test_is_some(self):
        x = Some(1)
        self.assertTrue(x.is_some())
        x = Null()
        self.assertFalse(x.is_some())

    def test_is_null(self):
        x = Some(1)
        self.assertFalse(x.is_null())
        x = Null()
        self.assertTrue(x.is_null())

    def test_expect(self):
        x = Some(1)
        self.assertEqual(x.expect("asdf"), 1)
        x = Null()
        with self.assertRaises(Exception):
            x.expect("asdf")

    def test_unwrap(self):
        x = Some(2)
        self.assertEqual(x.unwrap(), 2)
        x = Null()
        with self.assertRaises(Exception):
            x.unwrap()

    def test_unwrap_or(self):
        x = Some(2)
        self.assertEqual(x.unwrap_or(4), 2)
        x = Null()
        self.assertEqual(x.unwrap_or(4), 4)

    def test_unwrap_or_else(self):
        x = Some(2)
        self.assertEqual(x.unwrap_or_else(lambda: 4), 2)
        x = Null()
        self.assertEqual(x.unwrap_or_else(lambda: 4), 4)

    def test_map(self):
        x = Some("hello, world!")
        maybe_len = x.map(lambda s: len(s))
        self.assertEqual(maybe_len, Some(13))

        x = Null()
        maybe_len = x.map(lambda s: len(s))
        self.assertEqual(maybe_len, Null())

    def test_map_or(self):
        x = Some("foo")
        self.assertEqual(x.map_or(42, lambda v: len(v)), 3)
        x = Null()
        self.assertEqual(x.map_or(42, lambda v: len(v)), 42)

    def test_map_or_else(self):
        k = 21
        x = Some("foo")
        self.assertEqual(x.map_or_else(lambda: k * 2, lambda v: len(v)), 3)
        x = Null()
        self.assertEqual(x.map_or_else(lambda: k * 2, lambda v: len(v)), 42)

    def test_ok_or(self):
        x = Some("foo")
        self.assertEqual(x.ok_or(0), Ok("foo"))

        x = Null()
        self.assertEqual(x.ok_or(0), Err(0))

    def test_ok_or_else(self):
        x = Some("foo")
        self.assertEqual(x.ok_or_else(lambda: 0), Ok("foo"))

        x = Null()
        self.assertEqual(x.ok_or_else(lambda: 0), Err(0))

    def test_and_(self):
        x = Some(2)
        y = Null()
        self.assertEqual(x.and_(y), Null())

        x = Null()
        y = Some("foo")
        self.assertEqual(x.and_(y), Null())

        x = Some(2)
        y = Some("foo")
        self.assertEqual(x.and_(y), Some("foo"))

        x = Null()
        y = Null()
        self.assertEqual(x.and_(y), Null())

    def test_and_then(self):
        sq = lambda x: Some(x * x)
        nope = lambda x: Null()

        self.assertEqual(Some(2).and_then(sq).and_then(sq), Some(16))
        self.assertEqual(Some(2).and_then(sq).and_then(nope), Null())
        self.assertEqual(Some(2).and_then(nope).and_then(sq), Null())
        self.assertEqual(Null().and_then(sq).and_then(sq), Null())

    def test_filter(self):
        is_even = lambda n: n % 2 == 0

        self.assertEqual(Null().filter(is_even), Null())
        self.assertEqual(Some(3).filter(is_even), Null())
        self.assertEqual(Some(4).filter(is_even), Some(4))

    def test_or_(self):
        x = Some(2)
        y = Null()
        self.assertEqual(x.or_(y), Some(2))

        x = Null()
        y = Some(100)
        self.assertEqual(x.or_(y), Some(100))

        x = Some(2)
        y = Some(100)
        self.assertEqual(x.or_(y), Some(2))

        x = Null()
        y = Null()
        self.assertEqual(x.or_(y), Null())

    def test_or_else(self):
        nobody = lambda: Null()
        vikings = lambda: Some("vikings")

        self.assertEqual(Some("barbarians").or_else(vikings), Some("barbarians"))
        self.assertEqual(Null().or_else(vikings), Some("vikings"))
        self.assertEqual(Null().or_else(nobody), Null())
