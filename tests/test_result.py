from unittest import TestCase
from resopt import Some, Null, Ok, Err


class TestResult(TestCase):
    def test__eq(self):
        self.assertEqual(Ok(1), Ok(1))
        self.assertNotEqual(Ok(1), Ok(2))
        self.assertEqual(Err(1), Err(1))
        self.assertNotEqual(Err(1), Err(2))
        self.assertNotEqual(Ok(1), Err(1))

        o = Ok(1)
        e = Err(2)
        self.assertEqual(o, o)
        self.assertEqual(e, e)
        self.assertNotEqual(o, e)

    def test_is_ok(self):
        x = Ok(-3)
        self.assertTrue(x.is_ok())
        x = Err("err")
        self.assertFalse(x.is_ok())

    def test_is_err(self):
        x = Ok(-3)
        self.assertFalse(x.is_err())
        x = Err("err")
        self.assertTrue(x.is_err())

    def test_ok(self):
        x = Ok(2)
        self.assertEqual(x.ok(), Some(2))
        x = Err("Nothing here")
        self.assertEqual(x.ok(), Null())

    def test_err(self):
        x = Ok(2)
        self.assertEqual(x.err(), Null())
        x = Err("Nothing here")
        self.assertEqual(x.err(), Some("Nothing here"))

    def test_map(self):
        def maybe_int(x):
            try:
                return Ok(int(x))
            except Exception as e:
                return Err(e)

        double = lambda x: x * 2
        line = "1,2,3,4".split(",")
        expected = [2, 4, 6, 8]

        for exp, num in zip(expected, line):
            maybe_num = maybe_int(num).map(double)
            self.assertEqual(maybe_num, Ok(exp))

    def test_map_or_else(self):
        k = 21
        double = lambda _: k * 2
        length = lambda x: len(x)

        x = Ok("foo")
        self.assertEqual(x.map_or_else(double, length), 3)
        x = Err("foo")
        self.assertEqual(x.map_or_else(double, length), 42)

    def test_map_err(self):
        stringify = lambda e: f"error code: {e}"
        x = Ok(2)
        self.assertEqual(x.map_err(stringify), Ok(2))
        x = Err(13)
        self.assertEqual(x.map_err(stringify), Err("error code: 13"))

    def test_and_(self):
        x = Ok(2)
        y = Err("late error")
        self.assertEqual(x.and_(y), Err("late error"))

        x = Err("early error")
        y = Ok("foo")
        self.assertEqual(x.and_(y), Err("early error"))

        x = Err("not a 2")
        y = Err("late error")
        self.assertEqual(x.and_(y), Err("not a 2"))

        x = Ok(2)
        y = Ok("different result type")
        self.assertEqual(x.and_(y), Ok("different result type"))

    def test_and_then(self):
        sq = lambda x: Ok(x * x)
        err = lambda x: Err(x)

        self.assertEqual(Ok(2).and_then(sq).and_then(sq), Ok(16))
        self.assertEqual(Ok(2).and_then(sq).and_then(err), Err(4))
        self.assertEqual(Ok(2).and_then(err).and_then(sq), Err(2))
        self.assertEqual(Err(3).and_then(sq).and_then(sq), Err(3))

    def test_or_(self):
        x = Ok(2)
        y = Err("late error")
        self.assertEqual(x.or_(y), Ok(2))

        x = Err("early error")
        y = Ok(2)
        self.assertEqual(x.or_(y), Ok(2))

        x = Err("not a 2")
        y = Err("late error")
        self.assertEqual(x.or_(y), Err("late error"))

        x = Ok(2)
        y = Ok(100)
        self.assertEqual(x.or_(y), Ok(2))

    def test_or_else(self):
        sq = lambda x: Ok(x * x)
        err = lambda x: Err(x)

        self.assertEqual(Ok(2).or_else(sq).or_else(sq), Ok(2))
        self.assertEqual(Ok(2).or_else(err).or_else(sq), Ok(2))
        self.assertEqual(Err(3).or_else(sq).or_else(err), Ok(9))
        self.assertEqual(Err(3).or_else(err).or_else(err), Err(3))

    def test_unwrap_or(self):
        optb = 2
        x = Ok(9)
        self.assertEqual(x.unwrap_or(optb), 9)

        x = Err("error")
        self.assertEqual(x.unwrap_or(optb), optb)

    def test_unwrap_or_else(self):
        count = lambda x: len(x)

        self.assertEqual(Ok(2).unwrap_or_else(count), 2)
        self.assertEqual(Err("foo").unwrap_or_else(count), 3)

    def test_unwrap(self):
        x = Ok(2)
        self.assertEqual(x.unwrap(), 2)

        x = Err("emergency failure")
        with self.assertRaises(Exception):
            x.unwrap()

    def test_expect(self):
        x = Ok(2)
        self.assertEqual(x.expect("Asdf"), 2)

        x = Err("emergency failure")
        with self.assertRaises(Exception):
            x.expect("Testing expect")

    def test_unwrap_err(self):
        x = Ok(2)
        with self.assertRaises(Exception):
            x.unwrap_err()

        x = Err("emergency failure")
        self.assertEqual(x.unwrap_err(), "emergency failure")

    def test_expect_err(self):
        x = Ok(10)
        with self.assertRaises(Exception):
            x.expect_err("Testing expect_err")
