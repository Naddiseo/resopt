from __future__ import annotations

import typing
from typing_extensions import Protocol

__all__ = ["Ok", "Err", "Result"]

T = typing.TypeVar("T")
U = typing.TypeVar("U")
E = typing.TypeVar("E")
F = typing.TypeVar("F")
F1 = typing.Callable[[T], U]

if typing.TYPE_CHECKING:
    from typing import Any, Union, ClassVar
    from typing_extensions import Literal
    from .option import Option

    okfn = typing.Callable[[T], Any]
    errfn = typing.Callable[[E], Any]

_ok = object()
_err = object()


class Result(Protocol[T, E]):
    tag: ClassVar[Any]

    def is_ok(self) -> bool:
        pass

    def is_err(self) -> bool:
        pass

    def ok(self) -> Option[T]:
        pass

    def err(self) -> Option[E]:
        pass

    def map(self, op: F1[T, U]) -> Result[U, E]:
        pass

    def map_or_else(self, fallback: F1[E, U], op: F1[T, U]) -> U:
        pass

    def map_err(self, op: F1[E, F]) -> Result[T, F]:
        pass

    def and_(self, res: Result[U, E]) -> Result[U, E]:
        pass

    def and_then(self, op: F1[T, Result[U, E]]) -> Result[U, E]:
        pass

    def or_(self, res: Result[T, F]) -> Result[T, F]:
        pass

    def or_else(self, op: F1[E, Result[T, F]]) -> Result[T, F]:
        pass

    def unwrap_or(self, optb: T) -> T:
        pass

    def unwrap_or_else(self, op: F1[E, T]) -> T:
        pass

    def unwrap(self) -> T:
        pass

    def expect(self, msg: str) -> T:
        pass

    def unwrap_err(self) -> E:
        pass

    def expect_err(self, msg: str) -> E:
        pass


class Ok(Result[T, E]):
    tag = _ok
    value: T

    def __eq__(self, other) -> bool:
        return getattr(other, "tag", None) is _ok and self.value == other.value

    def __repr__(self) -> str:
        return f"Ok({repr(self.value)})"

    def __init__(self, value: T):
        self.value = value

    def is_ok(self) -> bool:
        return True

    def is_err(self) -> bool:
        return False

    def ok(self) -> Option[T]:
        from .option import Some

        return Some(self.value)

    def err(self) -> Option[E]:
        from .option import Null

        return Null()

    def map(self, op: F1[T, U]) -> Result[U, E]:
        return Ok(op(self.value))

    def map_or_else(self, fallback: F1[E, U], op: F1[T, U]) -> U:
        return self.map(op).unwrap_or_else(fallback)

    def map_err(self, op: F1[E, F]) -> Result[T, F]:
        return Ok(self.value)

    def and_(self, res: Result[U, E]) -> Result[U, E]:
        return res

    def and_then(self, op: F1[T, Result[U, E]]) -> Result[U, E]:
        return op(self.value)

    def or_(self, res: Result[T, F]) -> Result[T, F]:
        return Ok(self.value)

    def or_else(self, op: F1[E, Result[T, F]]) -> Result[T, F]:
        return Ok(self.value)

    def unwrap_or(self, optb: T) -> T:
        return self.value

    def unwrap_or_else(self, op: F1[E, T]) -> T:
        return self.value

    def unwrap(self) -> T:
        return self.value

    def expect(self, msg: str) -> T:
        return self.value

    def unwrap_err(self) -> E:
        raise Exception("called Result::unwrap_err ok Ok value")

    def expect_err(self, msg: str) -> E:
        raise Exception(msg)


class Err(Result[T, E]):
    tag = _err
    value: E

    def __eq__(self, other) -> bool:
        return getattr(other, "tag", None) is _err and self.value == other.value

    def __repr__(self) -> str:
        return f"Err({repr(self.value)})"

    def __init__(self, err: E):
        self.value = err

    def is_ok(self) -> bool:
        return False

    def is_err(self) -> bool:
        return True

    def ok(self) -> Option[T]:
        from .option import Null

        return Null()

    def err(self) -> Option[E]:
        from .option import Some

        return Some(self.value)

    def map(self, op: F1[T, U]) -> Result[U, E]:
        return Err(self.value)

    def map_or_else(self, fallback: F1[E, U], op: F1[T, U]) -> U:
        return self.map(op).unwrap_or_else(fallback)

    def map_err(self, op: F1[E, F]) -> Result[T, F]:
        return Err(op(self.value))

    def and_(self, res: Result[U, E]) -> Result[U, E]:
        return Err(self.value)

    def and_then(self, op: F1[T, Result[U, E]]) -> Result[U, E]:
        return Err(self.value)

    def or_(self, res: Result[T, F]) -> Result[T, F]:
        return res

    def or_else(self, op: F1[E, Result[T, F]]) -> Result[T, F]:
        return op(self.value)

    def unwrap_or(self, optb: T) -> T:
        return optb

    def unwrap_or_else(self, op: F1[E, T]) -> T:
        return op(self.value)

    def unwrap(self) -> T:
        raise Exception("called Result::unwrap on Err value")

    def expect(self, msg: str) -> T:
        raise Exception(msg)

    def unwrap_err(self) -> E:
        return self.value

    def expect_err(self, msg: str) -> E:
        return self.value


def match(thing: Result, ok: okfn, err: errfn) -> Any:
    if thing.tag is _ok:
        assert isinstance(thing, Ok)
        return ok(thing.value)
    elif thing.tag is _err:
        assert isinstance(thing, Err)
        return err(thing.value)


if __name__ == "__main__":
    pass
