from __future__ import annotations

import typing
from typing_extensions import Protocol

__all__ = ["Option", "Some", "Null"]

T = typing.TypeVar("T")
U = typing.TypeVar("U")
E = typing.TypeVar("E")
F = typing.Callable[[], T]
F1 = typing.Callable[[T], U]

if typing.TYPE_CHECKING:
    from typing import Union, ClassVar, Any
    from typing_extensions import Literal
    from .result import Result

_some = object()
_null = object()


class Option(Protocol[T]):
    tag: ClassVar[Any]

    def is_some(self) -> bool:
        pass

    def is_null(self) -> bool:
        pass

    def expect(self, msg: str) -> T:
        pass

    def unwrap(self) -> T:
        pass

    def unwrap_or(self, default: T) -> T:
        pass

    def unwrap_or_else(self, f: F[T]) -> T:
        pass

    def map(self, f: F1[T, U]) -> Option[U]:
        pass

    def map_or(self, default: U, f: F1[T, U]) -> U:
        pass

    def map_or_else(self, default: F[U], f: F1[T, U]) -> U:
        pass

    def ok_or(self, err: E) -> Result[T, E]:
        pass

    def ok_or_else(self, f: F[E]) -> Result[T, E]:
        pass

    def and_(self, optb: Option[U]) -> Option[U]:
        pass

    def and_then(self, f: F1[T, Option[U]]) -> Option[U]:
        pass

    def filter(self, f: F1[T, bool]) -> Option[T]:
        pass

    def or_(self, optb: Option[T]) -> Option[T]:
        pass

    def or_else(self, f: F[Option[T]]) -> Option[T]:
        pass


class Some(Option[T]):
    tag = _some

    def __eq__(self, other) -> bool:
        return getattr(other, "tag", None) is _some and self.value == other.value

    def __repr__(self) -> str:
        return f"Some({repr(self.value)})"

    def __init__(self, val: T):
        self.value = val

    def is_some(self) -> bool:
        return True

    def is_null(self) -> bool:
        return False

    def expect(self, msg: str) -> T:
        return self.value

    def unwrap(self) -> T:
        return self.value

    def unwrap_or(self, default: T) -> T:
        return self.value

    def unwrap_or_else(self, f: F[T]) -> T:
        return self.value

    def map(self, f: F1[T, U]) -> Option[U]:
        return Some(f(self.value))

    def map_or(self, default: U, f: F1[T, U]) -> U:
        return f(self.value)

    def map_or_else(self, default: F[U], f: F1[T, U]) -> U:
        return f(self.value)

    def ok_or(self, err: E) -> Result[T, E]:
        from .result import Ok

        return Ok(self.value)

    def ok_or_else(self, f: F[E]) -> Result[T, E]:
        from .result import Ok

        return Ok(self.value)

    def and_(self, optb: Option[U]) -> Option[U]:
        return optb

    def and_then(self, f: F1[T, Option[U]]) -> Option[U]:
        return f(self.value)

    def filter(self, f: F1[T, bool]) -> Option[T]:
        if f(self.value):
            return Some(self.value)
        else:
            return Null()

    def or_(self, optb: Option[T]) -> Option[T]:
        return self

    def or_else(self, f: F[Option[T]]) -> Option[T]:
        return self


class Null(Option[T]):
    tag = _null

    def __eq__(self, other) -> bool:
        return getattr(other, "tag", None) is _null

    def __repr__(self) -> str:
        return "Null"

    def is_some(self) -> bool:
        return False

    def is_null(self) -> bool:
        return True

    def expect(self, msg: str) -> T:
        raise Exception(f"cannot unwrap Null {msg}")

    def unwrap(self) -> T:
        raise Exception("called Option::unwrap on Null value")

    def unwrap_or(self, default: T) -> T:
        return default

    def unwrap_or_else(self, f: F[T]) -> T:
        return f()

    def map(self, f: F1[T, U]) -> Option[U]:
        return Null()

    def map_or(self, default: U, f: F1[T, U]) -> U:
        return default

    def map_or_else(self, default: F[U], f: F1[T, U]) -> U:
        return default()

    def ok_or(self, err: E) -> Result[T, E]:
        from .result import Err

        return Err(err)

    def ok_or_else(self, f: F[E]) -> Result[T, E]:
        from .result import Err

        return Err(f())

    def and_(self, optb: Option[U]) -> Option[U]:
        return Null()

    def and_then(self, f: F1[T, Option[U]]) -> Option[U]:
        return Null()

    def filter(self, f: F1[T, bool]) -> Option[T]:
        return Null()

    def or_(self, optb: Option[T]) -> Option[T]:
        return optb

    def or_else(self, f: F[Option[T]]) -> Option[T]:
        return f()


if __name__ == "__main__":
    some = Some(1)
    value = some.expect("asdf")
    assert value == 1
